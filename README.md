

# Wizard overrides for LibreWolf

## Introduction

This is my personal project which aims to primarily document the use of LibreWolf, which is a privacy focused fork of Firefox. This project started originally on GitHub, but I've migrated it to Codeberg instead.

I'm the creator of this entire project, but not without the help from many users' and friend's opinions. I encourage everybody to improve this project by recommending things to add, change, or remove in this project.

The best way to reach me through text or voice is by sending me a message regarding my project or questions related on:

- Discord: **evilwizard1041**

- Signal: **pinetree.92**




**My goals for this project:**

Include notes, accessible information, and personal configuration presets and guides of free and open source software and hardware that I recommend, either including, or unrelated to the main scope of this project.

Collectively and charitably improve public digital safety using video, picture, text tutorials, tips, and documentation of such things.

Promote software improvement by minimizing the environmentally impactful need or want to overbuild hardware due to encumbering proprietary software by encouraging light-weight, easy to learn, use, and manage software that also respects user freedom.




## Main Sources

**arkenfox user.js:** 
- **https://github.com/arkenfox/user.js**

- **https://github.com/arkenfox/user.js/wiki**

- **https://github.com/arkenfox/user.js/issues/946**

- **https://github.com/arkenfox/user.js/wiki/4.1-Extensions#small_orange_diamond-%EF%B8%8F-anti-fingerprinting-extensions-fk-no**

- **https://github.com/arkenfox/user.js/wiki/4.1-Extensions**

**LibreWolf:**
- **https://librewolf.net**

- **https://librewolf.net/docs/faq/**




## FAQ

I have limited my scope to open source browsers only for this project. Free and open source software is a better choice for end-user privacy and user freedom when compared to proprietary software counterparts. This FAQ also focuses on [**PrivacyTests'**](privacytests.org) listed browsers only.

Browser candidates in no particular order are as follows:

- [**Brave**](https://brave.com/)

- [**Firefox**](https://www.mozilla.org/en-US/firefox/new/)

- [**LibreWolf**](https://librewolf.net/)

- [**Mullvad**](https://mullvad.net/)

- [**Tor**](https://www.torproject.org/)

- [**Ungoogled Chromium**](https://github.com/ungoogled-software/ungoogled-chromium)




**Q: What about privacy-focused chromium browser forks like Brave?**

**A:** Brave is a for-profit company, which has and may continue to make bad for-profit changes contrary to what users want like Firefox.

>Source: https://wikiless.northboot.xyz/wiki/Brave_(web_browser)?lang=en#Business_model*

Chromium browsers had terrible fingerprint protection test results across the board, but as of a month or two ago, and as of June 10 2023 Brave started standing out as a privacy-focused browser that almost stacks up to Librewolf's test results. Brave browser still fails most all cross-session tracking tests unlike the next browsers discussed in the FAQ.

You should never use the Tor network connection feature in Brave, and never browse hidden service sites with Brave. Use Tor browser if you need to browse hidden service sites like .onion services.

The main reason of why I don't like Brave browser is their inclusion of annoying baked-in advertising, marketing, and feature bloat baked into the browser, a user who just wants a browser has features and services shoved in their face the second the open Brave browser for the first time.
In a way, this is a nail in the coffin of once again another for-profit company tarnishing otherwise good software.

For the time being of users that use Brave, I recommend they try LibreWolf.
It's possible that Brave may be a viable chromium browser as a backup due to not having removed support for MV2, which is a standard that allows users to use uBlock Origin if they so please.
Brave's inclusion of a built-in adblocker is a good feature, as the less extensions a user would be encouraged to add, the less unique those users could be, this is a plus.




**Q: Why do I recommend specifically LibreWolf for daily browsing? What about Mullvad, Tor, other Firefox forks?**

**A:** Librewolf's developers and maintainers have good practices and a non-profit team, they keep their fork of Firefox up to date, and focus on applying what other Firefox forks won't. LibreWolf, Mullvad's browser, and Tor browser's test results are documented in this website while lesser-known Firefox forks aren't.

>Source: https://privacytests.org/

Mullvad's browser includes Tor features from the Tor project, though they state they have no intention of profiting from their browser, and as critical as it may seem, I predict their extension entices users to purchase a subscription to their VPN.

Mullvad is a for-profit company that sells their VPN service.
By choosing to package their own VPN extension add-on into their non-profit browser, this encourages the user to switch to Mullvad VPN from other VPN services and stay in their ecosystem.

>Source: https://github.com/mullvad/browser-extension

The Mullvad browser extension has a few features that can be useful depending on how you browse, but it's only for Mullvad VPN. If you want to get those same features with a different VPN, you'd need a different method, extension, or VPN settings to do the same or similar thing, so in a way, Mullvad browser tries to sway you into their complete ecosystem.

>For more information: https://discuss.techlore.tech/t/for-people-using-the-mullvad-browser-extension-what-are-some-useful-things-you-can-do-with-it/8529

In an ideal world, the extension would be unnecessary to get the benefits of a VPN quicker and easier while browsing while using a browser.

Mullvad, arkenfox, and Librewolf provide almost or about the same default fingerprint protection to Tor if set to the "Standard" preset, but stepping up any higher from standard, "Safer" disables  , safest hard disables javascript, custom fonts, and other Tor-like fingerprint precausions, making it not very useful for clearweb browsing, since a large majority of websites hard require javascript to function enough for use.

Try [**this website's**](https://amiunique.org) test with a new untouched profile, you may get different results with and without javascript off. Your results may vary depending on which operating system you're on, your hardware, etc.

Mullvad intends for its users to not change the browser's settings at all, add site exceptions or extensions, just like the Tor browser for fingerprint protection.
This is inconvenient to most users, as usually they want to save bookmarks alongside a few site exceptions for saving cookies, and may want to save their session. Mullvad's browser is set to Firefox's private browsing mode, which means users generally don't have the option to change and modify their browser for expected features like session restore.

Mullvad browser includes most of the same functionality as the Tor browser, and as such shouldn't be modified at all. Both Mullvad and Tor are considered one-off browsers, but for different situations.
The main difference is that Mullvad browser does not include Tor hidden service connection capability, while Tor is designed for it. Using Tor to browse clearweb websites can make you stand out, since most websites require javascript, and you will have to reduce your Tor Security Level setting to Standard anyways.
The same can be said about Mullvad's same setting, which the user will need to leave at Standard to use their one-off websites like banking.

Mullvad seems to be a relatively trustworthy company, but they're still a for-profit company.

Tor browser is intended as a higher level of fingerprint protection than Mullvad, but simply put it's mostly focused on circumventing censorship.
 Do not use Tor browser with a VPN, do not use Tor browser to browse clearweb websites, and don't expect a VPN or Tor to be a silver bullet for privacy.



 Tor is very slow for daily browsing due to hopping through many obfuscation servers to reach the desired website you want, so again, it's only an option if you're actually trying to circumvent privacy.
i2pd is a better option for circumventing censorship, as long as the website you're trying to access is compatible.
https://i2pd.website/



**Q: Why Librewolf instead of Firefox + arkenfox user.js?**

**A:** Librewolf browser applies something very similar to arkenfox user.js already by default for the user.
Imagine your browser already being secure and private out of the box without user input.

1. You don't necessarily have to modify Librewolf, as the browser already has good security and privacy defaults out of the box

2. No advertising, tracking, or marketing out of the box

3. Non-profit, non-donation, free and open source developer team

4. Librewolf doesn't make any privacy disrespecting outbound connections you'd want to block unlike Firefox
(https://librewolf.net/docs/faq/#does-librewolf-make-any-outgoing-connections)

5. Up to date with Firefox's advances in privacy and security, and actually applies them!

6. Feature parity with most other browsers, just like Firefox.

I've personally used and read through the arkenfox project and have used it for a few years time with a similar experience as LibreWolf. I still use documentation it provides for learning, updating, and improving my own information while writing this project.

Mozilla, the maker of Firefox, even if their browser is open source, they continue to push advertising, tracking, and profit-focused updates to default Firefox that violate user privacy and usability.

For a first time Firefox user, this makes arkenfox not very practical, as users of the Firefox browser get their privacy violated after launching the default generated profiles by accident before first run of arkenfox.
Examples of these changes are Pocket, the default page you're presented ads in their browser, your default search engine being set to Google, and by opening a new tab will net you even more ads.
This is similar to Brave browser, how they annoy users with feature bloat, ads, and user tracking, and is in my opinion not acceptable.

For arkenfox, automatically removing and disabling Mozilla's Firefox defaults is not done automatically for the user, this requires the user to download and manually run two scripts which are included alongside the user.js, one for updating the user.js, and the other for automatically cleaning old prefs.








If you've made it this far, and chose to try Librewolf, what are some ways you can improve Librewolf for daily use?

I'll be narrowing on the scope of this guide to just common daily browsing.


### Settings

Changing settings of the browser at all could pose the risk of hurting fingerprint protection, privacy, and potential security of the browser.

I intended for my custom user.js script to have the goal of not decreasing the privacy/security properties of the browser, and not modifying the browser's fingerprint.
If you were concerned it was a possibility, you should leave your browser's profile as default as possible, and be weary of touching any browser settings until you read and approve the changes I've made within the included user.js file.

I pre-included extensions for convenience, these are in the custom profile's extensions folder. I have these disabled by default, as the user should acknowledge manually enabling them. Librewolf by default already includes uBlock Origin.

Paste about:settings into your browser's address bar or open the application menu on the right and click "Settings".


### Manage site cookie saving exceptions

Cookies are most commonly associated with website login keeping, website settings, and more. I'd suggest only adding websites you trust to keep logins for, and to avoid doing this for critical websites on devices that aren't physically secured from bad actors.
I'd recommend not enabling autologin for banking, or otherwise websites which if compromised by a real life thief could jeopardize your finances, private information, or could lead to identity fraud.

Librewolf by default does not save cookies or site data, I see this as a great default to keep. This encourages the user to acknowledge saving cookies for specific sites across sessions, and to keep the browser's cookie storage clean and organized no matter the user.

To add an exception to save data for a website you wish to keep cookies for, go to here:
```
Librewolf Settings > Privacy & Security > Manage Exceptions
```

I recommend you type links within Manage Exceptions' text box `https://` before them, without typing that it will create a `https://` and `http://` website url so that it creates one website instead of two.
For example:
```
click Address of website > type https://searx.be > click Allow > Save Changes
```
This is an example of how to save site login and settings for searx.be search engine website.


### Search Engine
Change your search engine to use an instance of [**SearX**](https://searx.github.io/searx/) instead of [**DuckDuckGo**](https://duckduckgo.com/).
LibreWolf includes [**searx.be**](https://searx.be/) as an included default search engine choice, but it's currently not the default active search engine.
I recommend you delete all of the other search engines and start fresh. Start with the included "SearXNG - searx.be" search engine option instead.

For some instances of searx, their hosters have their search location set to be a country outside the US, such as Germany.
```
In searx.be settings panel: Set preferred language to desired language.
```

DuckDuckGo is commonly the default search engine in most privacy focused browsers, however they practice censorship and are a proprietary for-profit company which provides centralised results. You would want to change it to something free and open source, as search engines are 50% of your browser's defense against malware.

Brave search engine is generally annoying to use. They tend to give frequent "Are you a robot?" checks, their searches aren't that fast, and they've started including ads. Brave isn't self-hostable either, even if it is free and open source.

It's wise to prefer open source licensed search engines which are self-hostable.
These search engines in no particular order are great choices for providing decentralized results:

[**araa**](https://github.com/Extravi/araa-search)
This one is my preferred search engine to use on a daily basis. The only downside is that this project isn't as popular, and as such, if there's currently downtime on all three instances, you'll have to use a different search engine.
At the time of writing, https://tailsx.com was the only fully working instance.

[**LibreX**](https://github.com/hnhx/librex)
This has lots of instances and is functionally about the same as araa-search.

[**SearXNG**](https://searx.space/)
SearXNG provides the option for users to privately aggregate as many search engines as they want, but may be an overwhelming amount of settings if you're a new user. This has the most instances out of all three of the search engines I've linked.


### Extensions

>Note: uBlock Origin is included in LibreWolf by default, and does not need to be added.

Adding and/or configuring extensions can make you stand out to fingerprinting scripts, as such you should consider not adding any or changing any at all.

The default filters and blocking of the included uBlock Origin extension is enough for most websites

Read [**this**](https://github.com/arkenfox/user.js/wiki/4.1-Extensions) before adding any new extensions that aren't on this list.

>Important note: I will be ignoring theming in this guide for extensions specifically, for extensions you are free to choose whatever you want for them as it shouldn't change privacy/security results.

I recommend not changing your browser's theme within Extensions and themes of Librewolf away from the defaults.
I'd suggest instead leaving the theme at "System theme - auto".
Instead, change your operating system's global light or dark theming.
I recommend leaving your home page as blank to reduce website connections at startup, and to just get used to the address bar to search with your search engine instead
I'll return to theming on a later date, but this is outside the guide. Princeton based attacks (if you accidentally type a wrong address) are dependent on your attention to URL's, and recognition of dirty URL's and clean URL's.
Librewolf already shows clean URL's in the address bar, but that doesn't necessarily mean the website you're going to is safe. 

Keep extensions to a minimum and use seperate clean throw-away browser profiles for websites such as banking.
This is mostly recommended from arkenfox and LibreWolf developers in their documentation.


[**uBlock Origin**](https://github.com/gorhill/uBlock):

- Description: Likely the best adblocker/content blocker extension there is, it can make the average webpage a lot less bloated with ads by allowing you to load only what you want from a page, and block what you don't want through lists and/or your element picker.

- Settings: Default settings are good out of the box and shouldn't be touched.

I recommend disabling javascript and disabling 3rd party connections by default within uBlock Origin is a good idea for advanced users, this is included in the pictures. You will by then need to and incrementally return permissions per-website most of the time to unbreak them, but this can provide an extra layer of security and privacy from malicious websites:

1. First section: Untick "Show the number of blocked requests on the icon" and "I am an advanced user" to the on state.

2. "Default behavior": See included UO beginner settings.png or UO advanced settings.png

3. Second section: I recommend enabling "Ignore generic cosmetic filters" if you use a low end device.

4. Select all filters: I recommend enabling every single filter list except "Regions, languages".

5. After that, pick and choose regional/language filters only per your country if you need them.

5. Import this whole blob of urls into custom user lists. I'm considering trimming how many I use and recommend:

>Optional filters for uBlock Origin:
>- https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt
>- https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt
>- https://www.fanboy.co.nz/enhancedstats.txt
>- https://git.fleebs.pw/fleebs/ublock-filters/raw/branch/master/discord

Credit to fleebs at fleebs.pw, this is a custom blocking script for discord.com. It makes the Discord web version less annoying to use.

> For more information:
>- https://github.com/gorhill/uBlock/wiki/Blocking-mode
>- https://github.com/arkenfox/user.js/wiki/4.1-Extensions
>- https://librewolf.net/docs/addons/









### Recommendations for optional extensions

These extensions below are optional, conditional, and are only recommended for daily use if you can benefit from them:


[**Skip Redirect**](https://github.com/sblask/webextension-skip-redirect)

- Description: This is the only extension fully recommended by the arkenfox team.
Bypasses annoying website redirect pages. It's safe to bypass these because they don't matter and usually only seek to collect more of your data inbetween entering the website. I use this extension for the remaining websites which don't have privacy frontends.

- Settings: default


[**LibRedirect**](https://github.com/libredirect/libredirect)

- Settings: import "libredirect-settings.json" which is included next to extensions.txt

This is a powerful browser extension that can help to reduce user activity on big-tech owned websites. These large commercial tracking websites include fingerprinting scripts that are used to accurately track users down to their identity and serve them ads. LibRedirect does this by providing alternative open source "front-ends" to those popular websites, for example routing users to piped.video instead of youtube.com.

> To learn more about popular privacy frontends: https://www.privacyguides.org/en/frontends

This tool can be a bit difficult to use, since specific frontends might not work sometimes, and can be complicated for new users. The main take-away is not visiting common tracking websites like Reddit or YouTube can provide huge gains in privacy and usability while browsing the internet.
This tool can be used to help reduce your activity and interaction on websites like YouTube and Reddit that intend to track you by redirecting you to popular privacy frontend" websites or apps, such as Redlib website or FreeTube app.


[**True Sight**](https://github.com/claustromaniac/detect-cloudflare-plus)

- Description: Notifies you if a page is using CDNs, which can help you if you wish to avoid them as much as conveniently possible. The idea of CDN's is great on paper, but generally bad in practice for end-user privacy, the companies that own the most common CDN's are Cloudflare and Amazon, they do not respect user privacy regardless of the website you're visiting. Think of CDN's as a man-in-the-middle that profits off tracking your IP across websites with their own CDN, and partnered CDN's.

This tool pairs nicely with LibRedirect, as it allows you to discover instances without direct user connections to CDN's. As long as 
Settings: default


[**FastForward**](https://github.com/FastForwardTeam/FastForward)

- Description: Circumvent sites which make you wait for 5 - 60 seconds while showing you ads (like adf.ly) 

  - Bypass sites that make you do something such as subscribing to a youtube channel, or following somebody on social media (e.g., show.co and sub2unlock.com)

  - Block sites that track information about you (such as bit.ly and t.co)

It also helps to prevent your IP address from being logged by such sites by blocking and bypassing them.
I've used this extension for a long time and it has saved me a lot of time trying to bypass adfly.

- Settings: default


[**Bypass Paywalls Clean**](https://github.com/bpc-clone/bypass-paywalls-firefox-clean)
This add-on allows you to read articles from many popular journalism sites which implement a paywall via. a supported website list.
- Settings: Default


[**Dark Reader**](https://github.com/darkreader/darkreader)

- Description: Makes site pages dark to help your eyes, especially useful for 90% of the internet that doesn't have a built in dark theme automatically enabled.

- Settings: Default settings are good out of the box, but I recommend turning it off manually for websites that have a built-in dark mode to save on resources and processing time.




>I'd suggest using a seperate password manager instead of Librewolf's built-in password manager to reduce copy+pasting and typing passwords often. For the sake of this guide, I recommend the KeePassXC password manager application + it's accompanying browser extension. If you use a different password manager, use their respective extension instead.

[**KeePassXC Password Manager**](https://keepassxc.org/)

[**KeePassXC Browser Extension**](https://github.com/keepassxreboot/keepassxc)

- Description: Secure password manager, set up alongside desktop application.

Pre-included password managers with web browsers are insecure. It's considered more secure to use password manager extensions to assist with managing many accounts and constant login switching. Generally, you shouldn't need to always need to have the KeePassXC application open all the time if you don't need to relogin or create accounts that day.

Using a strong, offline password manager like this one can be used to save unique passwords per online account, plus included TOTP auth within each online account if supported by those websites.

>Read more here: https://www.linux.org/threads/in-depth-tutorial-how-to-set-up-2fa-totp-with-keepassxc-aegis-and-authy.36577/




### Good Online Account Security Practice tips

1. Don't be messy with accounts online
I recommend in general that internet users close as many online accounts as conveniently possible, reducing unnecessary account bloat which can be a threat to your privacy and security. Once you've done that, focus on quality instead of quantity for security features. Make use of TOTP, which is a very strong anti-

2. Try not to leave accounts unattended
It's not a good idea to make burner accounts which you create and forget the logins of, as these can more easily be broken into. I also recommend against making and using social media accounts.

3. Don't name so many accounts the same
Making accounts with the same or similar usernames/names can help bad actors link information to find out your identity. Sharing immature account media such as showing the ownership of valuable items or personally identiable info publicly can increase your risk of being targeted both online and in some cases  even offline.

4. Don't overcomplicate passwords, make use of stronger options
Not all websites support passwords that don't involve special characters, but when they do, these kinds of passwords are a decent choice to boost entropy, the difficulty of cracking your password, while still making your passwords human readable.
https://xkcd.com/936/
Passwords are not very strong alone, so you really should use TOTP and physical keys if possible, which have real security value against online and offline attackers.
The KeePassXC application and extension have support for usb security keys to make stepping up your security easy.

5. Don't tell others your master password
The master password of KeePassXC should ideally be a completely offline password you type to access all of your online passwords within your offline password manager.
Your master password should be memorized by head, never written down, never used as an online password. Such master password must be difficult for an offline attacker to crack. It should only be that offline password manager database's password. 
To get to that level of memorization,  It may be challenging, taking you a long time to remember a good difficult password by head, but that would be the best master password you can create.

6. For login auth, ditch phone call/text, email, and in general, ditch second device auth
It is in my opinion quicker, more convenient, and more secure to use KeePassXC's built-in TOTP feature instead of a 2FA authenticator on a seperate device such as a mobile phone.

You shouldn't need to copy+paste your passwords to get into websites often once you've learned how to use KeePassXC and the KeePassXC browser extension's default semi-auto login feature setting.
This reduces the occurance and chance of having one of your copied passwords within your clipboard, which attackers can abuse.

I don't recommend secondary devices for 2FA, nor do I recommend email or phone number 2FA.
I also don't recommend saving session, site data, or passwords within browsers on portable devices as much as possible.
Storage of passwords, secondary device 2FA, and identifying media on mobile devices, such as phones and laptops, are a bad idea if you take them with you on the go. They're very easy to lose and get stolen.
In the wrong hands, your safety, identity, and accounts will be in danger.
Login managers, and at least most of your daily logins, should ideally be reserved for home computers which are at least physically defended against bad actors or unnoticed viewing of your personal information.


7. Check up on breached accounts and news about breached services
You can use [**this website**](https://haveibeenpwned.com/) to find out if accounts were tied to any of your emails which have been in public breaches.




### VPN

[**Mullvad**](https://mullvad.net/), [**IVPN**](https://www.ivpn.net/), and [**ProtonVPN**](https://protonvpn.com/) are in my opinion the only three good services.
They have open source VPN apps which are also available on Linux. I'm not going to say you should buy a VPN service, or setup a VPN on a rented out server container, because it's wise to learn how to efficiently use them to get their benefits.

>Source: https://www.privacyguides.org/en/vpn/#info

A common use-case for a VPN is to bypass censorship and country firewalls, but there are other free ways to do that same or similar thing for free, such as I2PD or TOR networks. You can try an alternative DNS server and use LibRedirect for bypassing most censorship.

There are many issues and common mistakes with VPN software to account for. To name one, it's the potential for IP address leakage.

1. Don't use custom DNS servers with a VPN connection active.

2. Disable DNS-over-HTTPS in your browser before using a VPN.

3. Don't use Tor browser with a VPN.

4. Bind your VPN to your torrent client.

5. Use VPN kill switches within software and the VPN software itself to prevent one case of leaking.

6. Regularly test for leaking
Read: https://mullvad.net/en/help/webrtc/
Turning WebRTC support from enabled to disabled is possibly necessary before using a VPN, Mullvad's test should notify the user
You can use [**this website**](https://mullvad.net/en/check) to test WebRTC leak, other VPN's, or Mullvad's VPN if you chose to use one
OVPN provides [**testing**](https://www.dnsleaktest.com/) for testing DNS leaking.

7. Make sure your IPv6 is secure while using a VPN, or disable it.

I will expand VPN documentation after I have more testing. In general, VPNs aren't a silver bullet for privacy, but your OPSec can help make the purchase of a monthly VPN service more useful for hiding your online traffic on public networks, home networks.




### DNS

Setting a custom DNS resolver is free compared to a paid VPN service, and it can be used to possibly bypass censorship and to an extent, your ISP's DNS tracking.

There are a host of many options, but I mostly recommend Quad9's public DNS service.
Do not alter your default DNS settings in your VPN, browser, or system's settings while using a VPN.

You should only preceed to change your DNS and DNS-over-HTTPS settings if you're not using a VPN.
Don't use DNS-over-HTTPS in your browser's network settings while using a VPN, your DNS is already tunneled through VPN services like Mullvad's VPN.

Don't change your VPN network profile's DNS settings within your OS or your browser, as your DNS resolution is already handled by the VPN's encrypted DNS service through use of their VPN connection.

If you're not using a VPN, changing your DNS provider to servers like Quad9's or your own will be an improvement in speed, privacy, and malware blocking over your ISP's provided DNS servers. You can find other public options than what I picked [**here**](https://wiki.archlinux.org/title/Alternative_DNS_services).

You can set these in the Network settings of most browsers. For your OS, search "Connection Preferences" in your settings panel for GNOME and KDE's, they're usually very similar to eachother. Linux Mint has a very similar standalone application called nm-connection-editor installed by default, it's also available in most base repositories if you don't have any of these desktops.

NextDNS is probably my preferred featureful pick, but to simplify this guide, try Quad9 if you were using a different DNS provider, such as Google's or Cloudflare's.

Set your DNS settings in your operating system's profile to Quad9 if you aren't using a VPN instead of your ISP's preincluded ones:

- IPv4 DNS for Quad9's public DNS service:

  - Method: Automatic (DHCP) addresses only 9.9.9.9, 149.112.112.112

- IPv6 for Quad9's public DNS service

  - Method: Automatic, addresses only 2620:fe::fe, 2620:fe::9

There's other DNS and DNS-over-HTTPS services than Quad9 available, I just chose what I recommend.

Test your settings changes to make sure using [**service provided websites**](https://on.quad9.net/).

You can use tests which are commonly found on DNS and VPN provider's websites to be sure your settings are working.

Consider self-hosting and using your own DNS server.


### IPv6

The IPv6 part of this guide is very complicated and work in process. This is for users to reference if they need to use IPv6 but it's not set up to be private by default. You can skip this section if you've decided to disable IPv6.

>Read this: https://librewolf.net/docs/faq/#how-do-i-disable-ipv6

Unless you know how to configure your system's IPv6 connectivity to be private, it's a good idea to disable IPv6 connectivity in your system, or preferably your router if possible.

Disabling IPv6 in your system's connection settings, browser, and possibly router settings is easier than configuring it, this has no real reprocussions for home users. IPv6 is commonly recommended to be disabled in the past because it usually doesn't have private settings by default in many Linux distributions. IPv6 is also not exactly necessary for daily use. IPv6 without the right secure settings can possibly expose your MAC address to each connection, and cause IPv6 leak for some VPN's that don't route it alongside IPv4. Currently Mullvad can route IPv6 itself.

Either disabling or properly securing your IPv6 settings for both your normal connection profile and your VPN connection profile will prevent Mac address leak. I personally recommend people disable it on Linux to save time. Most people will never notice it's gone.

>View your system's current IPv6 setting here after closing and reopening your browser: https://mullvad.net/en/check


**My current easiest reproducable method for enabling IPv6:**

In the IPv6 section of your desktop's Connection settings: IPv6 privacy extensions: Enabled (prefer temporary address)

This setting is where I don't fully understand what to change it to or if to change it.

IPv6 address generation mode: Stable privacy

This setting seems to be enabled by default on most distributions while the above may not always be.

>Sources: 
>- https://wiki.archlinux.org/title/IPv6#Privacy_extensions
>- https://tldp.org/HOWTO/Linux+IPv6-HOWTO/ch06s05.html
>- https://vk5tu.livejournal.com/57160.html?nojs=1


### DNS-over-HTTPS

Enable DNS over HTTPS if not using a VPN, and preferably should be from the same provider as the DNS service you're using above.
Using DNS-over-HTTPS cannot be done in the system but can be done in most browsers.

In LibreWolf, go to:
```
General > Privacy Settings > DNS over HTTPS > Click "Max Protection", and in "Custom" paste 
Use Provider: Custom
https://dns.quad9.net/dns-query
```

There are other forms of DNS encryption methods that I'm still looking into. 
Consider self-hosting and using your own DNS-over-HTTPS server.

> For reading and answering common DNS-over-HTTPS questions:
>- https://www.quad9.net/news/blog/doh-with-quad9-dns-servers/
>- https://mullvad.net/en/help/dns-over-https-and-dns-over-tls/

Self hosting your own DNS-over-HTTPS server will be much faster and more private.


### Router and Internet

If you use VPN's in your house, don't change your DNS settings from the default in your router unless you know what you're doing. If possible, disable your router's ipv6 connectivity unless you know how to configure ipv6 to be private. It's also a good idea to do the same thing in your system settings and applications.

A system-wide hosts file is generally a good idea. A Pi-Hole server will be much better.


### Extra Notes

Open source passwordless login usb security key. 

> For more information: (https://youtube.com/watch?v=ze2i9V1_aIc)

- https://onlykey.io/ - In my opinion this is probably a better option for physical offline TOTP, it's very close to a Yubikey for compatibility and ease of use. Usb security keys are most commonly used in work environments, but with the nature of one of these keys, I recommend you only keep necessary work profiles on it. There's also nitrokey or solokey which seem lacking in features comparatively. This would probably be better for a work environment than KeePassXC TOTP alone.

I recommend these channels for more information about any complex topics mentioned in this guide.

- https://youtube.com/channel/UC7YOGHUfC1Tb6E4pudI9STA - Mental Outlaw

- https://youtube.com/channel/UCjr2bPAyPV7t35MvcgT3W8Q - The Hated One

- https://youtube.com/channel/UC2eYFnH61tmytImy1mTYvhA - Luke Smith

- https://youtube.com/channel/UC7YOGHUfC1Tb6E4pudI9STA - Chris Titus Tech

Librewolf is just one piece of software that sets a good example of providing a better service for free, rather than than what other popular browsers such as Brave do to annoy users.
(https://wikiless.northboot.xyz/wiki/Free_software?lang=en#Naming_and_differences_with_open_source)